var queue;
var stage;

var STAGE_WIDTH = 728;
var STAGE_HEIGHT = 90;


function init(){

	if (!Enabler.isInitialized()){
		Enabler.addEventListener(studio.events.StudioEvent.INIT, enablerInitialized);
	}else{
		enablerInitialized();
	}
}

function enablerInitialized(){
	Enabler.removeEventListener(studio.events.StudioEvent.INIT, enablerInitialized);
	if (!Enabler.isVisible()){
		Enabler.addEventListener(studio.events.StudioEvent.VISIBLE, isPageLoaded);
	}else{
		isPageLoaded();
	}
}

function isPageLoaded(){
	Enabler.removeEventListener(studio.events.StudioEvent.VISIBLE, isPageLoaded);
	if (Enabler.isPageLoaded()) {
    	adVisible();
  	}else {
    	Enabler.addEventListener(studio.events.StudioEvent.PAGE_LOADED, adVisible);
  }
}

function adVisible(){
	Enabler.removeEventListener(studio.events.StudioEvent.PAGE_LOADED, adVisible);
	preloadImages();
}

function preloadImages(){
	var path = "assets/"
	queue = new createjs.LoadQueue(false);
	queue.on('complete', handleComplete, this);
	queue.on('error', errorHandler, this);

	queue.loadManifest([
	 {id: 'upgrade_to', src: path+ 'upgrade_to.png' },
	 {id: 'image_stack', src: path+ 'image_stack.png' },
     {id: 'headline1', src: path+ 'headline1.png' },
     {id: 'headline1_mask', src: path+ 'headline1_mask.png'},
     {id: 'headline2', src: path+ 'headline2.png' },
     {id: 'headline2_mask', src: path+ 'headline2_mask.png'},
     {id: 'final_text', src: path+ 'final_text.png'},
     {id: 'final_text_mask', src: path+ 'final_text_mask.png'},
     {id: 'final_upgrade', src: path+ 'final_upgrade.png' },
     {id: 'final_upgrade_mask', src: path+ 'final_upgrade_mask.png'},
     {id: 'skymovies_logo', src: path+ 'skymovies_logo.png'},
     {id: 'skymovies_logo_mask', src: path+ 'skymovies_logo_mask.png'},
     {id: 'sky_glasslogo', src: path+ 'sky_glasslogo.png'},
     {id: "cta", src: path+'cta.png'},
     {id: "cta_mask", src: path+'cta_mask.png'}
 ]);
}

function handleComplete(event){
	console.log("all loaded");
	document.getElementById('canvas').style.background = 'none';
	stage = new createjs.Stage('canvas');
	createjs.Ticker.addEventListener('tick', handleTick);


	setF1();
	prepareLegals();
}

function setF1(){
	var upgrade_to = new createjs.Bitmap(queue.getResult('upgrade_to'));
		upgrade_to.name = 'upgrade_to';
	var skymovies_logo = new createjs.Bitmap(queue.getResult('skymovies_logo'));
	var image_stack = new createjs.Bitmap(queue.getResult('image_stack'));
	var sky_glasslogo = new createjs.Bitmap(queue.getResult('sky_glasslogo'));

	var containerF1 = new createjs.Container();

		image_stack.name = 'image_stack';

	console.log('image_stack.getBounds().width: ' + image_stack.getBounds().width);
	console.log('image_stack.getBounds().height: ' + image_stack.getBounds().height);

	console.log('upgrade_to.getBounds().width: ' + upgrade_to.getBounds().width);
	console.log('upgrade_to.getBounds().height: ' + upgrade_to.getBounds().height);

	console.log('skymovies_logo.getBounds().width: ' + skymovies_logo.getBounds().width);
	console.log('skymovies_logo.getBounds().height: ' + skymovies_logo.getBounds().height);

	upgrade_to.cache(0,0,113,22);
	skymovies_logo.cache(0,0,155,29);
	image_stack.cache(0,0,147,82);

	upgrade_to.x = 53;
	upgrade_to.y = 35;

	image_stack.y = 7;
	image_stack.x = 415;

	skymovies_logo.x = 200;
	skymovies_logo.y = 30;

	sky_glasslogo.x = 627;
	sky_glasslogo.y = 19;

	containerF1.addChild(image_stack, upgrade_to, skymovies_logo, sky_glasslogo);

	stage.addChild(containerF1);


	console.log("stage"+ stage.getChildByName('image_stack'));

	createjs.Tween.get(image_stack).wait(3000).to({alpha:0}, 1000);
	createjs.Tween.get(upgrade_to).wait(3000).to({alpha:0}, 1000);
	createjs.Tween.get(skymovies_logo).wait(3000).to({alpha:0}, 1000).call(setF2);
}


function setF2(){
	stage.removeAllChildren();
	stage.alpha = 1;
	var skymovies_logo = new createjs.Bitmap(queue.getResult('skymovies_logo'));

	var foraLimitedTime = new createjs.Bitmap(queue.getResult('headline1'));
	var foraLimitedTimeMask = new createjs.Bitmap(queue.getResult('headline1_mask'));

	var extra = new createjs.Bitmap(queue.getResult('headline2'));
	var extraMask = new createjs.Bitmap(queue.getResult('headline2_mask'));

	var sky_glasslogo = new createjs.Bitmap(queue.getResult('sky_glasslogo'));

	foraLimitedTime.name = 'foraLimitedTime';
	foraLimitedTime.x = foraLimitedTimeMask.x =  55;
	foraLimitedTime.y = foraLimitedTimeMask.y = 27;
	foraLimitedTime.alpha = 0;

	extra.name = 'extra';
	extra.x = 425;		extraMask.x = 425;
	extra.y = 27;		extraMask.y = 27;
	extra.alpha = 0;	extraMask.alpha = 0;

	skymovies_logo.x = 200;
	skymovies_logo.y = 30;
	skymovies_logo.alpha = 0;

	sky_glasslogo.x = 627;
	sky_glasslogo.y = 19;

	var extraGlow = new createjs.Bitmap(queue.getResult('headline2_mask'));
		extraGlow.x = 425;
		extraGlow.y = 27;
		extraGlow.alpha = 0;

	var extraGlowMask = new createjs.Shape();
		extraGlowMask.graphics.beginFill('red').drawRect(0,0,20,160);
		extraGlowMask.rotation = 15;
		extraGlowMask.x = extraGlow.x -100;
		extraGlowMask.y = extraGlow.y * 0.5;
		extraGlow.mask = extraGlowMask;

	stage.addChild(foraLimitedTime);
	stage.addChild(skymovies_logo);
	stage.addChild(extra);
	stage.addChild(extraGlow);
	stage.addChild(skymovies_logo);
	stage.addChild(sky_glasslogo);

	var headline1 = new createjs.Bitmap(queue.getResult('headline1'));
		headline1.x = 55;
		headline1.y = 27;
		headline1.alpha = 0;
		stage.addChild(headline1);
		createjs.Tween.get(headline1).to({alpha:1},500);

	var headline1Glow = new createjs.Bitmap(queue.getResult('headline1_mask'));
		headline1Glow.x = 64;
		headline1Glow.y = 22;
		stage.addChild(headline1Glow);

	var headline1GlowMask = new createjs.Shape();
		headline1GlowMask.graphics.beginFill('red').drawRect(0,0,20,40);
		headline1GlowMask.rotation = 15;
		headline1GlowMask.x = headline1.x - 100;
		headline1GlowMask.y = headline1.y * 0.5;
		headline1Glow.mask = headline1GlowMask;

	createjs.Tween.get(stage.getChildByName('foraLimitedTime')).wait(0).to({alpha:1},1000);
	createjs.Tween.get(extraGlow).wait(0).to({alpha:1},0);

	createjs.Tween.get(stage.getChildByName('extra')).wait(1000).to({alpha:1},1000);

	createjs.Tween.get(skymovies_logo).wait(1000).to({alpha:1},1000).call(function(){
		createjs.Tween.get(extraGlowMask).wait(500).to({x: extra.x + 253},1000);
	});

	createjs.Tween.get(headline1GlowMask).wait(1500).to({x: headline1.x + 190},1000).wait(0);
	createjs.Tween.get(stage).wait(4000).to({alpha:0}, 1000).call(animateF3);
}

function animateF3(){
	stage.removeAllChildren();
	stage.alpha = 1;

	var skymovies_logo 		= new createjs.Bitmap(queue.getResult('skymovies_logo'));
	var skymovies_logo_mask = new createjs.Bitmap(queue.getResult('skymovies_logo_mask'));

	var whenUpgrade 		= new createjs.Bitmap(queue.getResult('final_upgrade'));
	var whenUpgrade_mask 	= new createjs.Bitmap(queue.getResult('final_upgrade_mask'));

	var final_text 			= new createjs.Bitmap(queue.getResult('final_text'));
	var final_text_mask 	= new createjs.Bitmap(queue.getResult('final_text_mask'));

	var cta 				= new createjs.Bitmap(queue.getResult('cta'));
	var cta_mask 			= new createjs.Bitmap(queue.getResult('cta_mask'));

	var sky_glasslogo = new createjs.Bitmap(queue.getResult('sky_glasslogo'));
		sky_glasslogo.x = 627;
		sky_glasslogo.y = 19;

	skymovies_logo.x = 24;		skymovies_logo_mask.x 	= 24;
	skymovies_logo.y = 28;		skymovies_logo_mask.y 	= 28;
	skymovies_logo.alpha = 0;	skymovies_logo.alpha 	= 0;

	final_text.x = 190;		final_text_mask.x = 190;
	final_text.y = 27;		final_text_mask.y = 27;
	final_text.alpha = 0;	final_text.alpha = 0;

	whenUpgrade.x = 218;		whenUpgrade_mask.x = 218;
	whenUpgrade.y = 52;			whenUpgrade_mask.y = 52;
	whenUpgrade.alpha = 0;		whenUpgrade_mask.alpha = 0;

	cta.x = 488;				cta_mask.x = 488;
	cta.y = 10;					cta_mask.y = 10;
	cta.alpha = 1;				cta_mask.alpha = 1;

	var ctaText = new createjs.Text("Take this offer", "20px SkyTextRegular", "#fff");
		ctaText.lineWidth = 90;
		ctaText.textBaseline = "alphabetic";
		ctaText.textAlign = 'center';

		ctaText.x = cta.x + (ctaText.getBounds().width / 1.27);
		ctaText.y = cta.y + (ctaText.getBounds().height / 1.1);
		ctaText.alpha = 1;


	var lockUp3 = new createjs.Container();
	lockUp3.name = 'lockUp3';
	lockUp3.alpha = 0;


	stage.addChild(skymovies_logo);
	stage.addChild(skymovies_logo_mask);


	stage.addChild(final_text);
	stage.addChild(final_text_mask);

	stage.addChild(whenUpgrade);
	stage.addChild(whenUpgrade_mask);

	stage.addChild(cta);
	stage.addChild(ctaText);
	stage.addChild(cta_mask);

	stage.addChild(sky_glasslogo);

	var glowMaskUpgrade = new createjs.Shape();
		glowMaskUpgrade.graphics.beginFill('red').drawRect(0,0,20,40);
		glowMaskUpgrade.rotation = 15;
		glowMaskUpgrade.x = whenUpgrade.x - 20;
		glowMaskUpgrade.y = whenUpgrade.y - 10;

	var glowMaskMovies= new createjs.Shape();
		glowMaskMovies.graphics.beginFill('red').drawRect(0,0,20,40);
		glowMaskMovies.rotation = 15;
		glowMaskMovies.x = skymovies_logo.x - 20;
		glowMaskMovies.y = skymovies_logo.y - 10;

	var glowMaskfinalText= new createjs.Shape();
		glowMaskfinalText.graphics.beginFill('red').drawRect(0,0,20,90);
		glowMaskfinalText.rotation = 15;
		glowMaskfinalText.x = final_text.x - 20;
		glowMaskfinalText.y = final_text.y - 10;

	var cta01Mask = new createjs.Shape();
		cta01Mask.graphics.beginFill('rgba(255,0,0,.3)').drawRoundRect(0,0,119,72,10);
		cta01Mask.x = cta.x;
		cta01Mask.y = cta.y;

	var glare = new createjs.Shape();
		glare.graphics.beginLinearGradientFill(['rgba(255,255,255,.5)','rgba(255,255,255,.8)','rgba(255,255,255,.5)'], [0,.5,1], 0, 0 , 10, 10).drawRect(0,0,10,50);
		glare.rotation = 15;
		glare.y = cta.y - 10;
		glare.x = cta.x - 20;

	var glareBlurFilter = new createjs.BlurFilter(10,2,1);
		glare.filters = [glareBlurFilter];

	var bounds = glareBlurFilter.getBounds();
	setTimeout(function(){
		glare.cache(-200+bounds.x, -100+bounds.y, 400+bounds.width, 250+bounds.height);
	},200);

	stage.addChild(glare);
	glare.mask = cta01Mask;

	var moviesMask = new createjs.Shape();
		moviesMask.graphics.beginFill('rgba(255,0,0,.3)').drawRoundRect(0,0,130,45,10);
		moviesMask.x = skymovies_logo.x;
		moviesMask.y = skymovies_logo.y;

	var glare2 = new createjs.Shape();
		glare2.graphics.beginLinearGradientFill(['rgba(255,255,255,.5)','rgba(255,255,255,.8)','rgba(255,255,255,.5)'], [0,.5,1], 0, 0 , 5, 5).drawRect(0,0,10,50);
		glare2.rotation = 15;
		glare2.y = skymovies_logo.y - 10;
		glare2.x = skymovies_logo.x - 20;

	var glareBlurFilter2 = new createjs.BlurFilter(10,2,0);

	glare.filters = [glareBlurFilter2];

	var bounds = glareBlurFilter2.getBounds();
	setTimeout(function(){
		glare2.cache(-200+bounds.x, -100+bounds.y, 400+bounds.width, 250+bounds.height);
	},200);

	stage.addChild(glare2);
	glare2.mask = moviesMask;

	skymovies_logo_mask.mask = glare2;
	final_text_mask.mask = glowMaskfinalText;
	whenUpgrade_mask.mask = glowMaskUpgrade;
	cta_mask.mask = glare;

	console.log('final_text.getBounds().width ' + final_text.getBounds().width);
	console.log('whenUpgrade.getBounds().width ' + whenUpgrade.getBounds().width);

	createjs.Tween.get(skymovies_logo_mask).wait(0).to({alpha:1},1000);
	createjs.Tween.get(skymovies_logo).wait(0).to({alpha:1},1000);
	createjs.Tween.get(final_text).wait(0).to({alpha:1},1000);
	createjs.Tween.get(final_text_mask).wait(0).to({alpha:1},1000);
	createjs.Tween.get(glowMaskfinalText).wait(1000).to({x: final_text.x + final_text.getBounds().width + 40},1000);
	createjs.Tween.get(whenUpgrade_mask).wait(1000).to({alpha:1},1000);
	createjs.Tween.get(whenUpgrade).wait(1000).to({alpha:1},1000).call(function(){
	createjs.Tween.get(glowMaskUpgrade).wait(1000).to({x: whenUpgrade.x + whenUpgrade.getBounds().width +10},1000).call(function(){
		createjs.Tween.get(ctaText).wait(0).to({alpha:1},1000);
		createjs.Tween.get(cta).wait(0).to({alpha:1},1000).call(function(){
			createjs.Tween.get(glare).wait(0).to({x:cta.x + 200},1000).call(function(){
				stage.removeChild(glare);
				});
			});
		});
	});
}

function handleTick(){
	stage.update();
}


function errorHandler(){
	console.log('error loading');
}

function exit(){
	Enabler.exit("Exit was clicked");
	Enabler.counter("Clicked exit");
}

function prepareLegals(){
	var cross = document.getElementById('cross');
	var legals = document.getElementById('legals');
	var clicked = false;
	legals.style.opacity = 1;

	var legalContent = document.getElementById('legalsContainer');

	legals.onclick = (function(){
		if (!clicked){
			TweenLite.to(legalContent, .5, { ease: Power1.easeOut, y: - (legalContent.offsetHeight), onComplete: completeFunc});
			clicked = true;
			legals.innerHTML = '';
		}else{
			TweenLite.to(legalContent, .5, { ease: Power1.easeOut, y: 0, onComplete: completeFunc});
			clicked = false;
			legals.innerHTML = '';
		}

		function completeFunc(){
			if(clicked){
				legals.innerHTML = 'Click to hide legals';
			}else{
				legals.innerHTML = 'Click to see legals';
			}
		}
	});
	
	function completeFunc(){
		TweenLite.to(legals, 0, {opacity:"1"});
	}
}

window.onload = init();
